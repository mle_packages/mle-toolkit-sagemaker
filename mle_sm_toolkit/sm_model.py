# Setup clients
import boto3
import sagemaker


def get_session(region_name="us-east-1", profile_name=None):
    """
    Returns boto3 session
    """
    try:
        if profile_name:
            session = boto3.Session(region_name=region_name, profile_name=profile_name)
        else:
            session = boto3.Session(region_name=region_name)
        return session
    except Exception as e:
        print("Failed to get boto3 session")
        print(e)
        raise e


def create_model_group(
    name="sm-model-group", description="Model Group for SM Toolkit", client=client
):
    """
    Create Model Registry Group
    """
    try:
        # Create Model Registry Group
        model_package_group_input_dict = {
            "ModelPackageGroupName": name,
            "ModelPackageGroupDescription": description,
        }
        create_model_pacakge_group_response = client.create_model_package_group(
            **model_package_group_input_dict
        )
        print(
            "Model Group created successfully - ModelPackageGroup Arn : {}".format(
                create_model_package_group_response["ModelPackageGroupArn"]
            )
        )
    except Exception as e:
        print("Model Group creation failed")
        print(e)
        raise e


def register_model(
    name="sm-model-group",
    framework_version="1.5-1",
    model_url="s3://sagemaker-us-east-1-123456789012/sagemaker/DEMO-xgboost-churn/output/xgboost-2020-11-11-16-18-17-568/output/model.tar.gz",
    auc_score=None,
    accuracy=None,
    client=client,
):
    """
    Registers the model to SageMaker Model Registry.
    """
    try:
        # Register Model
        image_uri = sagemaker.image_uris.retrieve(
            "xgboost", "us-east-1", framework_version
        )
        modelpackage_inference_specification = {
            "InferenceSpecification": {
                "Containers": [{"Image": image_uri, "ModelDataUrl": model_url}],
                "SupportedContentTypes": ["text/csv"],
                "SupportedResponseMIMETypes": ["text/csv"],
            }
        }
        create_model_package_input_dict = {
            "ModelPackageGroupName": name,
            "ModelPackageDescription": "Last training model with auc score: {:.2f} and accuracy: {:.2f}".format(
                (auc_score), (accuracy)
            ),
            "ModelApprovalStatus": "PendingManualApproval",
        }
        create_model_package_input_dict.update(modelpackage_inference_specification)
        create_model_package_response = client.create_model_package(
            **create_model_package_input_dict
        )
        print(
            "Model registered successfully - ModelPackageArn : {}".format(
                create_model_package_response["ModelPackageArn"]
            )
        )
    except Exception as e:
        print("Model registration failed")
        print(e)
        raise e
