import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mle_sm_toolkit",
    version="0.0.1",
    author="Marcos E Soto",
    author_email="marcos.esteban.soto@gmail.com",
    description="Sagemaker Mle Toolkit",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/experiments65/mle-toolkit-sagemaker",
    packages=setuptools.find_packages(),
    # install req from requirements.txt
    install_requires=[
        req for req in open("requirements.txt").read().split("\n") if req
    ],
)
